import ezdxf
import numpy as np
import matplotlib.pyplot as plt
import fire
import copy

inches_per_meter = 39.3701

def read(filename:str ):

    np.set_printoptions(suppress = True)
#filename = "/Users/dennis.melamed/kitware_projects/AVIT/data/3D_20140115_123620.dxf"
    doc = ezdxf.readfile(filename)
    points = []

    for p in doc.entitydb.query("POINT"):
        points.append(p.dxf.location)
    points = np.array(points) * inches_per_meter


    x_axis = copy.deepcopy(points[1,:])
    x_axis = x_axis / np.linalg.norm(x_axis)
    y_axis = copy.deepcopy(points[2,:]) #- points[0,:]
    y_axis = y_axis / np.linalg.norm(y_axis)
    z_axis = np.cross(x_axis, y_axis)
    correction_mat = np.stack((x_axis, y_axis, z_axis), axis = -1)


    points = (correction_mat.T @ points.T).T

    print("Coordinate axis points: ")
    print(points[:3,:])

    print("Other measured points: ")
    print(points[3:,:])


    fig, axs = plt.subplots(1,3)
    axs[0].scatter(points[:,0], points[:,1])
    axs[0].set_xlabel("X")
    axs[0].set_ylabel("Y")
    axs[1].scatter(points[:,0], points[:,2])
    axs[1].set_xlabel("X")
    axs[1].set_ylabel("Z")
    axs[2].scatter(points[:,1], points[:,2])
    axs[2].set_xlabel("Y")
    axs[2].set_ylabel("Z")

    for ax in axs:
        ax.set_aspect('equal')
    plt.show()

if __name__ == '__main__':
    fire.Fire(read)
