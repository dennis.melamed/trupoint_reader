# Using the trupoint for AVIT

## Collecting points relative to the origin

### required hardware:
- trupoint 300 w/ base
- microusb cable
- usb-A to usb-C adapter, if your machine only has usb-C ports
- trupoint charger (or any usb-A charger)
- computer, network connection required for initial setup (pip installs) but afterwards processing is offline


### Using the device

- Turn on using red power/fire button
- Hit FUNC key
- bottom row central column will be an icon that looks like a bunch of points on a folded sheet, wiht the sublabel DXF, and a red ML on the bottom. Select using the arrow keys and = key
- Calibrate:
    - Make the device level with the ground (i.e. both bubble levels are centered)
    - Turn device 90 deg CW (until green part of displayed circle dissapears)
    - Repeat 90 deg turn
    - you may have to repeat a step if the device moves while the loading icon is on the screen
- Measure a point:
    - point # is shown on screen (P#)
    - Hit red fire button to turn on laser
    - Point at target
    - CAREFUL, this can cause a reboot. The software isn't great: You can hit the camera w/ crosshair button to see what the imager on the front of the rangefinder sees, can help locate point at a distance
    - Hit the touchscreen button just above the fire button to record the point (looks like the points on a folded sheet icon)

- Points to measure (see figure):
    - To set coordinate axes:
        - Center of aruco
        - Right edge of aruco, in line with center
        - Top edge of aruco, in line with center
    - Defect centroids



- Hit button on upper right of pad (to the right of red power/fire button) to save (the one below the save icon)

### Software

- create virtualenv (`python -m venv trupoint_venv`) in this directory
- `pip install -r requirements.txt`
- Use: `python read_file.py path_to_trupoint_file.dxf`


### To get points in aruco frame
- Plug trupoint into computer, access TPt300 drive > DXF Exports > Folder with collection date/time 
- Copy `3D_{date}.dxf` to computer storage, run `read_file.py` on the file to get points in the aruco frame
    - Points stored in the dxf are NOT ARUCO FRAME - the rangefinder makes the line between the 1st and 2nd points you collect the Z axis, then defines the remaining axes based on the horizontal leveling procedure done at the start.  `read_file.py` fixed this to the aruco coordinate frame based on the first 3 points.
    - read_file will also output a plot of the points



## Determining tag poses relative to each other
- Procedure is similar to above, but instead of collecting defect centroids after setting the coordinate frame, collect the remaining aruco tags:
    - record the center, right and top points of each new fiducial, same as the origin one. 
    - Be sure to record the order of the tag ids you collect (e.g. 2 is origin, 4 is collected next, then 5, then 3, and so on)
- Pass the 3D_{date}.dxf file generated to `generate_aruco_string_for_rtabmap.py` as:
    - `python generate_aruco_string_for_rtabmap.py 3D_{date}.dxf 2,4,5,3` where the numbers after the filename are the aruco tag ids you collected, in the appropriate order
- output will be a string that looks like `{tag_id 1} x y z r p y| {tag_id 2} x y z r p y| ...` 
    - e.g. for a system consisting of tags 2 and 4:  
    - `2 0.00000, 0.00000 0.00000 0.00000 0.00000 0.00000|4 3.78539, 0.57103 1.05289 0.19739 -1.54314 -0.25483`
- Copy this string into `config.ini` under the `Markers/Priors` key - do not comment out what is already there, remove it (save elsewhere if needed) and replace it (otherwise throws an error)
