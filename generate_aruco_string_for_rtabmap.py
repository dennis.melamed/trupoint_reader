import ezdxf
import numpy as np
import matplotlib.pyplot as plt
import fire
import copy
from scipy.spatial.transform import Rotation as R



def create_matrix(point_0, point_1, point_2):
    x_axis = copy.deepcopy(point_1 - point_0)
    x_axis = x_axis / np.linalg.norm(x_axis)
    y_axis = copy.deepcopy(point_2 - point_0) 
    y_axis = y_axis / np.linalg.norm(y_axis)
    z_axis = np.cross(x_axis, y_axis)
    correction_mat = np.stack((x_axis, y_axis, z_axis), axis = -1)
    return correction_mat


def read(filename:str , marker_ids : list[int]):

    np.set_printoptions(suppress = True)
    doc = ezdxf.readfile(filename)
    points = []

    for p in doc.entitydb.query("POINT"):
        points.append(p.dxf.location)
    points = np.array(points)



    origin_frame = create_matrix(points[0,:], points[1,:], points[2,:])
    correction_mat = origin_frame


    points = (correction_mat.T @ points.T).T

    tag_string = []
    for tag_idx in range(points.shape[0]//3):
        tag = points[tag_idx*3 : tag_idx*3 + 3,:]
        rot_mat = create_matrix(tag[0,:], tag[1,:], tag[2,:])
        pos_offset = tag[0,:]
        sci_rot = R.from_matrix(rot_mat)
        rpy_rot = sci_rot.as_euler('xyz')

        pos_offset[np.abs(pos_offset ) < 1e-3] = 0
        rpy_rot[np.abs(rpy_rot ) < 1e-3] = 0

        tag_string.append(f"{marker_ids[tag_idx]} {pos_offset[0]:.5f}, {pos_offset[1]:.5f} {pos_offset[2]:.5f} {rpy_rot[0]:.5f} {rpy_rot[1]:.5f} {rpy_rot[2]:.5f}")
    tag_string_complete = "|".join(tag_string)
    print(tag_string_complete)


if __name__ == '__main__':
    fire.Fire(read)
